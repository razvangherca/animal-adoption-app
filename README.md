## Animal adoption app (Bachelor degree prorject)

### Problem: 
Millions of animals are currently in shelters and foster homes awaiting adoption. Create an experience that will help connect people looking for a new pet with the right companion for them. Help an adopter find a pet which matches their lifestyle, considering factors including breed, gender, age, temperament and health status.

### Potential solution: 
Design an app that, based on the profile of the user it can show him potential pets from shelters or other owners from his area. Both the users and the pets have certain typologies so we will have to make a matchmaking service so both sides are happy. The app should also include tips for correctly taking care of a specific breed, possibility of making it a community based app so people can help each other. Also, if you already have an animal or you just can’t have an animal in your home, some shelters or regular people receive donations for taking care of animals. So make sure to make your animal profile attractive and you might make your life easier to take care of the ‘unlucky’ souls out there. Concept-level questions for the user profile configuration (these will help with categorizing each individual user into a specific type of adopter):
Do you have any pets at home?
How much time are you away from home?
Do you have kids?
Do you have a job?
How much money can you invest monthly into raising a pet?
Do you live in an apartment or a house?
What kind of pets do you like?
Do you get angry easily?
Do you like long walks?
Do you have enough spare time?
	...and many more.
After answering these question, the user’s profile will receive a badge that will help him find pets that will suit his lifestyle, won’t harm the pet and of course, won’t make his life harder. At the end of the day we must satisfy both of the parts involved in this process.

### Persona: 

Name: Victor Popescu

Personal background:
Age: 31;
Status: Married, expecting a baby;
Education: Master’s degree in Computer Science;

Professional background: 
Occupation: Full-time software developer in an outsourcing company;
Income: 28000 € per year;

User environment:
Location: mostly at work, but also at home;
Devices: his phone, sometimes on the laptop;

Psychographics:
Enjoys long walks in the park or at the mountains;
Fascinated about wildlife;
Always had a soft spot for pets;
Hates the fact that stray dogs don’t receive love;

End goals:
Use animal-adoption app to find the perfect pet that he and his family can take care of, preferably a pet that gets well with kids;
Scenario:
“I’m tired of people who try and buy only the most expensive animal breeds. We should try and realize that each soul needs a warm shelter and love. I’m working a 9 to 5 job and me and my wife are expecting a baby boy in a couple of months. We would really love to adopt a soul that can cope with being alone ⅓ of the day and is caring with little kids. We don’t care if its fur isn’t perfect or if it isn’t the prettiest.”

### Pros: 
There’s no such service in Iasi and there are a few in our country but they are mostly copies of Olx or Publi24 or pla###in old Facebook Groups. Our service will be designed so each animal, no matter the type or breed will be taken care of by the perfect owner for its personality.

### Cons:
 To be honest I could not find a con to this project, because it’s all about helping the community and the animals in them.


### Technology stack

I’ve decided to use the MERN stack. This includes React.js for the Frontend part, Node.js & Express for the Backend side and MongoDB for the database. Of course I’ll probably make use of some public Apis also such as GoogleMaps or Geolocation or create some of my own, depending on the evolution of the project.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

