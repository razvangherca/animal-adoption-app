import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { CSSTransition } from "react-transition-group";
import { useParams } from "react-router-dom";
import { AuthContext } from "../../shared/context/auth-context";
import { useHttpClient } from "../../shared/hooks/http-hook";

import Card from "../../shared/components/UIElements/Card";

const Result = (props) => {
  const auth = useContext(AuthContext);
  const userId = useParams().userId;
  const history = useHistory();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const goToProfile = () => {
    history.push("/users/" + auth.userId);
  };

  const editUserType = async () => {
    try {
      const response = await sendRequest(
        `http://localhost:5000/api/users/${userId}/quiz`,
        "PATCH",
        JSON.stringify({
          userType: props.quizResult,
        }),
        {
          "Content-Type": "application/json",
          Authorization: "Bearer " + auth.token,
        }
      );
      const tokenExpirationDate = new Date(
        new Date().getTime() + 1000 * 60 * 60
      );
      localStorage.setItem(
        "userData",
        JSON.stringify({
          userId: userId,
          userType: response.user.userType,
          token: response.token,
          expiration: tokenExpirationDate.toISOString(),
        })
      );
      auth.setUserType(response.user.userType);
      auth.setToken(response.token);
      auth.setTokenExpirationDate(tokenExpirationDate);
      goToProfile();
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    editUserType();
  }, [props.quizResult]);

  return (
    <Card className="quiz-container-card">
      <CSSTransition
        className="container result"
        component="div"
        transitionName="fade"
        transitionEnterTimeout={800}
        transitionLeaveTimeout={500}
        transitionAppear
        transitionAppearTimeout={500}
      >
        <div>
          Your user profile type is: <strong>{props.quizResult}</strong>!
        </div>
      </CSSTransition>
    </Card>
  );
};

export default Result;
