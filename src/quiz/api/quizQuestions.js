var ownerProfileQuizQuestions = [
    {
        question: "How many hours of the day are you working/busy?",
        answers: [
            {
                type: "Ideal Owner",
                content: "4-5 hours/day"
            },
            {
                type: "Good Owner",
                content: "8-9 hours/day"
            },
            {
                type: "Not-so-good Owner",
                content: "10+ hours/day"
            }
        ]
    },
    {
        question: "Do you have kids? If so, how many?",
        answers: [
            {
                type: "Ideal Owner",
                content: "No kids"
            },
            {
                type: "Good Owner",
                content: "1-2 kids"
            },
            {
                type: "Not-so-good Owner",
                content: "3+"
            }
        ]
    },
    {
        question: "How much money can you invest per month for the well-being of your pet?",
        answers: [
            {
                type: "Ideal Owner",
                content: "200$+"
            },
            {
                type: "Good Owner",
                content: "100-200$"
            },
            {
                type: "Not-so-good Owner",
                content: "0-100$"
            }
        ]
    },
    {
        question: "What kind of home do you have?",
        answers: [
            {
                type: "Ideal Owner",
                content: "A house with a yard"
            },
            {
                type: "Good Owner",
                content: "Big apartment (>60 m²)"
            },
            {
                type: "Not-so-good Owner",
                content: "Small apartment (<60 m²)"
            }
        ]
    },
    {
        question: "Does your family/friends like pets?",
        answers: [
            {
                type: "Ideal Owner",
                content: "Very much!"
            },
            {
                type: "Good Owner",
                content: "Yes!"
            },
            {
                type: "Not-so-good Owner",
                content: "Not really"
            }
        ]
    },
    {
        question: "Would you take your pet on vacations?",
        answers: [
            {
                type: "Ideal Owner",
                content: "If it has all the conditions for a good time, of course!"
            },
            {
                type: "Good Owner",
                content: "Of course, everywhere!"
            },
            {
                type: "Not-so-good Owner",
                content: "Not really."
            }
        ]
    },
    
    
  ];
  
  export default ownerProfileQuizQuestions;