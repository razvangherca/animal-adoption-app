import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import Users from "./user/pages/Users";
import NewPet from "./pets/pages/NewPet";
import MainNavigation from "./shared/components/Navigation/MainNavigation";
import UserPets from "./pets/pages/UserPets";
import UpdatePet from "./pets/pages/UpdatePet";
import Auth from "./user/pages/Auth";
import Pets from "./pets/pages/Pets";
import Profile from "./user/pages/Profile";
import UpdateProfile from "./user/pages/UpdateProfile";
import QuizPage from "../src/quiz/pages/QuizPage";
import ContactOwner from "../src/user/pages/ContactOwner";
import Home from "../src/user/pages/Home";
import { AuthContext } from "./shared/context/auth-context";
import { useAuth } from "./shared/hooks/auth-hook";

import "./App.scss";

const App = () => {
  const { token, userType, setUserType, login, logout, userId, setToken, setTokenExpirationDate } = useAuth();
  let routes;

  if (token) {
    routes = (
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/pets" exact>
          <Pets />
        </Route>
        <Route path="/users" exact>
          <Users />
        </Route>
        <Route path="/users/:userId/editProfile" exact>
          <UpdateProfile />
        </Route>
        <Route path="/users/:userId/quiz" exact>
          <QuizPage />
        </Route>
        <Route path="/:userId/pets" exact>
          <UserPets />
        </Route>
        <Route path="/pets/new" exact>
          <NewPet />
        </Route>
        <Route path="/pets/:petId/contactOwner">
          <ContactOwner />
        </Route>
        <Route path="/pets/:petId">
          <UpdatePet />
        </Route>
        <Route path="/users/:userId">
          <Profile />
        </Route>
        <Redirect to="/" />
      </Switch>
    );
  } else {
    routes = (
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/pets" exact>
          <Pets />
        </Route>
        {/* <Route path="/users" exact>
          <Users />
        </Route> */}
        <Route path="/:userId/pets" exact>
          <UserPets />
        </Route>
        <Route path="/auth">
          <Auth />
        </Route>
        <Redirect to="/" />
      </Switch>
    );
  }

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn: !!token,
        token: token,
        userType: userType,
        setUserType: setUserType,
        userId: userId,
        login: login,
        logout: logout,
        setToken,
        setTokenExpirationDate,

      }}
    >
      <Router>
        <MainNavigation />
        <main>{routes}</main>
      </Router>
    </AuthContext.Provider>
  );
};

export default App;
