import React from "react";

import Card from "../../shared/components/UIElements/Card";
import PetItem from "./PetItem";
import Button from "../../shared/components/FormElements/Button";

import "./PetList.scss";

const PetList = (props) => {
  if (props.items.length === 0) {
    return (
      <div className="pet-list center">
        <Card>
          <h2>No pets found. Maybe add one?</h2>
          <Button to="/pets/new">Add a pet</Button>
        </Card>
      </div>
    );
  }

  return (
    <ul className="pet-list">
      {props.items.map((pet) => (
        <PetItem
          key={pet.id}
          id={pet.id}
          image={pet.image}
          name={pet.name}
          description={pet.description}
          breed={pet.breed}
          breedCareSuggestions={pet.breedCareSuggestions}
          userType={pet.userType}
          address={pet.address}
          creatorId={pet.creator}
          coordinates={pet.location}
          onDelete={props.onDeletePet}
        />
      ))}
    </ul>
  );
};

export default PetList;
