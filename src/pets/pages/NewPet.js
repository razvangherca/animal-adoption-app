import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import Select from "react-select";

import Input from "../../shared/components/FormElements/Input";
import Button from "../../shared/components/FormElements/Button";
import {
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH,
} from "../../shared/util/validators";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ImageUpload from "../../shared/components/FormElements/ImageUpload";
import { useForm } from "../../shared/hooks/form-hook";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";
import "./PetForm.scss";

const userTypeOptions = [
  { value: "Ideal Owner", label: "Ideal Owner" },
  { value: "Good Owner", label: "Good Owner" },
  { value: "Not-so-good Owner", label: "Not so good Owner" },
];

const userTypeSelectStyle = {
  control: (base) => ({
    ...base,
    border: "1px solid #cccccc",
    boxShadow: "none",
    "&:hover": {
      border: "1px solid #cccccc",
    },
  }),
  option: (styles, { isFocused }) => {
    return {
      ...styles,
      backgroundColor: isFocused ? "#FCB9B2" : null,
      color: "#333333",
    };
  },
};

const NewPet = () => {
  const auth = useContext(AuthContext);
  const { isLoading, error, sendRequest, clearError } = useHttpClient();
  const [formState, inputHandler] = useForm(
    {
      name: {
        value: "",
        isValid: false,
      },
      description: {
        value: "",
        isValid: false,
      },
      breed: {
        value: "",
        isValid: false,
      },
      breedCareSuggestions: {
        value: "",
        isValid: false,
      },
      userType: {
        value: "",
        isValid: false,
      },
      address: {
        value: "",
        isValid: false,
      },
      image: {
        value: null,
        isValid: false,
      },
    },
    false
  );

  const history = useHistory();

  const petSubmitHandler = async (event) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("name", formState.inputs.name.value);
      formData.append("description", formState.inputs.description.value);
      formData.append("breed", formState.inputs.breed.value);
      formData.append(
        "breedCareSuggestions",
        formState.inputs.breedCareSuggestions.value
      );
      formData.append("userType", formState.inputs.userType.value);
      formData.append("address", formState.inputs.address.value);
      formData.append("image", formState.inputs.image.value);
      await sendRequest("http://localhost:5000/api/pets", "POST", formData, {
        Authorization: "Bearer " + auth.token,
      });
      console.log(formData);
      history.push("/");
    } catch (err) {}
  };

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      <form className="pet-form" onSubmit={petSubmitHandler}>
        {isLoading && <LoadingSpinner asOverlay />}
        <Input
          id="name"
          element="input"
          type="text"
          label="Name"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid title."
          onInput={inputHandler}
        />
        <Input
          id="description"
          element="textarea"
          label="Description"
          validators={[VALIDATOR_MINLENGTH(5)]}
          errorText="Please enter a valid description (at least 5 characters)."
          onInput={inputHandler}
        />
        <Input
          id="breed"
          element="input"
          type="text"
          label="Breed"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid breed"
          onInput={inputHandler}
        />
        <Input
          id="breedCareSuggestions"
          element="textarea"
          label="Breed care suggestions"
          validators={[VALIDATOR_MINLENGTH(5)]}
          errorText="Please enter some care tips that the future owner might need."
          onInput={inputHandler}
        />
        <p className="ownerTypeLabel">Owner Type</p>
        <Select
          className="select-user-type"
          options={userTypeOptions}
          onChange={(event) => {
            formState.inputs.userType.value = event.value;
            formState.inputs.userType.isValid = true;
          }}
          placeholder="Select your desired owner type.."
          styles={userTypeSelectStyle}
        />
        <Input
          id="address"
          element="input"
          type="text"
          label="Address"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid address."
          onInput={inputHandler}
        />
        <ImageUpload
          id="image"
          onInput={inputHandler}
          errorText="Please provide an image."
        />
        <Button type="submit" disabled={!formState.isValid}>
          ADD PET
        </Button>
      </form>
    </React.Fragment>
  );
};

export default NewPet;
