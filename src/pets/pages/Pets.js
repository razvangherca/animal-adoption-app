import React, { useEffect, useState, useContext } from "react";

import PetList from "../components/PetList";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";
import Modal from "../../shared/components/UIElements/Modal";
import Button from "../../shared/components/FormElements/Button";

const Pets = () => {
  const [showQuizModal, setShowQuizModal] = useState(true);
  const auth = useContext(AuthContext);
  const [loadedPets, setLoadedPets] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  useEffect(() => {
    const fetchPets = async () => {
      try {
        const responseData = await sendRequest(
          `http://localhost:5000/api/pets/`,
          "GET",
          null,
          {
            "Content-Type": "application/json",
            Authorization: "Bearer " + auth.token,
          }
        );
        setLoadedPets(responseData.pets);
      } catch (err) {}
    };
    fetchPets();
  }, [sendRequest, auth.token]);

  const petDeletedHandler = (deletedPetId) => {
    setLoadedPets((prevPets) =>
      prevPets.filter((pet) => pet.id !== deletedPetId)
    );
  };
  const closeQuizModalHandler = () => setShowQuizModal(false);

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className="center">
          <LoadingSpinner />
        </div>
      )}
            {auth.isLoggedIn && (
        <Modal
          show={showQuizModal}
          onCancel={closeQuizModalHandler}
          header={"Keep in mind!"}
          contentClass="pet-item__modal-content"
          footerClass="pet-item__modal-actions"
          footer={
            <React.Fragment>
              <Button inverse to={`/users/${auth.userId}`}>
                PROFILE
              </Button>
              <Button onClick={closeQuizModalHandler}>CLOSE</Button>
            </React.Fragment>
          }
        >
          <p className="quiz-modal">
            Before browsing our pets keep in mind that you must take the Owner
            Quiz to determine your user type. After taking it, you will be able
            to contact the owners of the pets matching your profile.
          </p>
        </Modal>
      )}
      {!isLoading && loadedPets && (
        <PetList
          items={loadedPets.filter((pet) => pet.creator !== auth.userId)}
          onDeletePet={petDeletedHandler}
        />
      )}
    </React.Fragment>
  );
};

export default Pets;
