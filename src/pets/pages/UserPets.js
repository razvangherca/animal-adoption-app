import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import PetList from "../components/PetList";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import { useHttpClient } from "../../shared/hooks/http-hook";

const UserPets = () => {
  const [loadedPets, setLoadedPets] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const userId = useParams().userId;

  useEffect(() => {
    const fetchPets = async () => {
      try {
        const responseData = await sendRequest(
          `http://localhost:5000/api/pets/user/${userId}`
        );
        setLoadedPets(responseData.pets);
      } catch (err) {}
    };
    fetchPets();
  }, [sendRequest, userId]);

  const petDeletedHandler = (deletedPetId) => {
    setLoadedPets((prevPets) =>
      prevPets.filter((pet) => pet.id !== deletedPetId)
    );
  };

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className="center">
          <LoadingSpinner />
        </div>
      )}
      {!isLoading && loadedPets && (
        <PetList items={loadedPets} onDeletePet={petDeletedHandler} />
      )}
    </React.Fragment>
  );
};

export default UserPets;
