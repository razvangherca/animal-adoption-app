import React, { useEffect, useState, useContext } from "react";
import { useParams } from "react-router-dom";

import Avatar from "../../shared/components/UIElements/Avatar";
import Card from "../../shared/components/UIElements/Card";
import Button from "../../shared/components/FormElements/Button";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";

import "./Profile.scss";

const Profile = () => {
  const auth = useContext(AuthContext);
  const [loadedUser, setLoadedUser] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const userId = useParams().userId;

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const responseData = await sendRequest(
          `http://localhost:5000/api/users/${userId}`
        );
        setLoadedUser(responseData.user[0]);
      } catch (err) {}
    };
    fetchUser();
  }, [sendRequest, userId]); //don't set auth in the array, it will crash

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className="center">
          <LoadingSpinner />
        </div>
      )}
      {!isLoading && loadedUser && (
        <div className="user-profile__container">
          <Card className="user-profile">
            <div className="user-profile__image">
              <Avatar
                image={`http://localhost:5000/${loadedUser.image}`}
                alt={loadedUser.name}
              />
            </div>
            <div className="user-profile__info">
              <h2>Name: {loadedUser.name}</h2>
              <br />
              <h4>Email: {loadedUser.email}</h4>
              <br />
              <h4>
                {loadedUser.pets.length}{" "}
                {loadedUser.pets.length === 1 ? "Pet" : "Pets"}
              </h4>
              {loadedUser.userType === "UndeterminedProfile" ? (
                <h5>Your adopter profile is unknown. Please take the quiz.</h5>
              ) : (
                <h5>Owner type: {loadedUser.userType}</h5>
              )}
            </div>
            <div className="user-profile__actions">
              {loadedUser.pets.length !== 0 ? (
                <Button inverse to={`/${loadedUser.id}/pets`}>
                  MY PETS
                </Button>
              ) : (
                <Button disabled>MY PETS</Button>
              )}
              <Button to={`/users/${loadedUser.id}/editProfile`}>EDIT</Button>
              {loadedUser.userType === "UndeterminedProfile" ? (
                <Button inverse to={`/users/${loadedUser.id}/quiz`}>
                  TAKE QUIZ
                </Button>
              ) : (
                <Button inverse to={`/users/${loadedUser.id}/quiz`}>
                  RETAKE QUIZ
                </Button>
              )}
            </div>
          </Card>
        </div>
      )}
    </React.Fragment>
  );
};

export default Profile;
