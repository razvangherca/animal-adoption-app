import React, { useContext } from "react";

import Card from "../../shared/components/UIElements/Card";
import Button from "../../shared/components/FormElements/Button";
import { AuthContext } from "../../shared/context/auth-context";

import "./Home.scss";

const Home = () => {
  const auth = useContext(AuthContext);
  return (
    <Card className="home-article">
      <h2>Welcome!</h2>
      <h4>
        We were driven by the soul purpose to help people and animals get
        together easier and make eachothers life better. We believe that each
        pet has its own behaviour, the same as humans. Therefor, we came up with
        an app that connects you with a perfect friend, that matches your
        personality and lifestyle. You will be able to find out what kind of pet
        owner you are by trying out our quiz and then find only pets that suit
        you.
      </h4>
      <br />
      {!auth.isLoggedIn && <Button to="/auth">Join us</Button>}
      {auth.isLoggedIn && (
        <h4>
          If you haven't configured your profile or you changed something in
          your lifestyle..
        </h4>
      )}
      {auth.isLoggedIn && (
        <Button to={`/users/${auth.userId}/quiz`}>Take our quiz</Button>
      )}
      <br />
      {auth.isLoggedIn && <h4>If you're all set up..</h4>}
      {auth.isLoggedIn && (
        <Button to={`/pets`}>Check the available pets</Button>
      )}
      <br />
      <h4>
        <span className="keepinmind">Keep in mind: </span>Our app is a bridge
        between humans and pets. So the connection must be ideal. After taking
        the quiz, you'll be in one of the 3 owner type categories: Ideal owner
        (a big house, enough spare time, enough money to spend), Good owner
        (decent size house, enough money and moderate amount of time) and "Not
        so good" owner (Small house, not so much money). These results may vary
        depending on your answers so make sure you give them properly.
      </h4>
      <br />
    </Card>
  );
};

export default Home;
