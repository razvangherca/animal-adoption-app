import { createContext } from "react";

export const AuthContext = createContext({
  isLoggedIn: false,
  userId: null,
  setUserType: null,
  userType: null,
  token: null,
  login: () => {},
  logout: () => {},
  setToken: null,
  setTokenExpirationDate: null,
});
